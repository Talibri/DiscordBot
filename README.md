# DiscordBot

A RedBot cog for Talibri!

#Current features:

* user_info:
Fetches crafting and gathering user data based off of their userid, then outputs it in Discord.

* `buy_price` and `sell_price`:
Parses the market for the given string(s) and gives you the best BO or SO, respectively.

TO INSTALL:

1. Install and configure RedBot made by TwentySix using the following instructions:
https://twentysix26.github.io/Red-Docs/red_install_windows/

2. Once installed and able to run on your Discord server, drop the `talibri.py` file into the `cogs` folder.

3. Type `[p]load talibri`

4. Type `[p]restart`

Any questions, please contact cdgraves on Talibri Discord: @cdgraves#2822 or email: asdfdelta@gmail.com