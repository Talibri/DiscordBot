import requests
import discord
import httplib2
from discord.ext import commands
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
import os
import re

try: # check if BeautifulSoup4 is installed
    from bs4 import BeautifulSoup
    soupAvailable = True
except:
    soupAvailable = False
try:
    from tabulate import tabulate
    tabulateAvailable = True
except:
    tabulateAvailable = False

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Google Sheets API Python Quickstart'


url_types = ['sell', 'buy']
filterProfNames = [
    "Chef", "Smelter", "Smith", "Planer", "Crafter", "Mixologist", "Tailor"
]

class Talibri:
    def __init__(self, bot):
        self.bot = bot

    #General commands
    @commands.command()
    async def shrug(self):
        """ Be apathetic about it! """
        await self.bot.say("¯\_(ツ)_/¯")


    @commands.command()
    async def bite(self, user : discord.Member):
        """ ARNGH, ARNGH, ARNGH """
        await self.bot.say("I don't know how I supposed to bite without teeth...")

    #Talibri-specific commands
    @commands.group(pass_context=True)
    async def talibri(self, ctx):
        """This is a list of queries you can perform"""
        #if ctx.invoked_subcommand is None:
        #    await send_cmd_help(ctx)

    @talibri.command(name="user_info")
    async def user_info(self, userid):
        if userid == "765":
            await self.bot.say("Canhuck is a troublemaker.")
        playerResults = ["Player Results\n"]
        email = '' //REPLACE WITH CREDENTIALS
        password = ''
        r = requests.get('https://talibri.com/users/sign_in')
        soup = BeautifulSoup(r.text, 'html.parser')
        utf = soup.find("input", attrs={'name': 'utf8'})['value']
        token = soup.find("input", attrs={'name': 'authenticity_token'})['value']
        cookies = r.cookies
        payload = {
            'user[email]': email,
            'user[password]': password,
            'user[remember_me]': '0',
            'commit': 'Log in',
            'utf8': utf,
            'authenticity_token': token
        }
        r = requests.post('https://talibri.com/users/sign_in', data=payload, cookies=cookies)
        if(r.status_code == 200):
            await self.bot.say("Logged in, fetching user information.")
        cookies = r.cookies
        # Fetch Name
        url = "https://talibri.com/user/{}/profile/".format(str(userid))
        r = requests.get(url, cookies=cookies)
        soup = BeautifulSoup(r.text, 'html.parser')
        csrf = soup.find("meta", {"name":"csrf-token"})['content']
        etag = r.headers["ETag"]
        cookies2 = r.cookies
        rightNav = soup.find("div", class_='alert alert-success text-center')
        ch1 = rightNav.contents[1]
        name = ch1.contents[0]
        description = "This is provided by the TalibriBot."
        embed = discord.Embed(colour=0xFF0000, description=description)
        embed.title = "Player profile"
        embed.set_author(name=name, url=url)
        #newResults = "``` " + str(name)
        # Fetch skills
        url2 = "https://talibri.com/user/{}/skills/".format(str(userid))
        r2 = requests.get(url2, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies2)
        parse = r2.text.split(".html(")[1].split(");")[0].replace("\\","")
        extrasoup = BeautifulSoup(parse, 'html.parser')
        skills = extrasoup.find_all("div", class_='panel-heading')
        embedCounter = 0
        embedCounterExceeded = False
        for skill in skills:
            if(len(skill.contents) > 4):
                skillName = skill.contents[1].contents[0]
                level = skill.contents[2].split(": ")[1].split("n")[0]
                experiencePercent = skill.contents[3].contents[1].contents[1].contents[0]
                masteryId = int(skill.contents[1]['href'].split("masteries/")[1])
                if(level == "Level 1"):
                    continue
                embedValue = ""
                embedName = "{} ({})".format(skillName, level)
                ## 'Click' on the skill to get a list of Masteries for the current skill`
                masteryUrl = "https://talibri.com/users/{}/masteries/{}".format(str(userid), str(masteryId))
                masteryR = requests.get(masteryUrl, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies2)
                hotsoup = BeautifulSoup(masteryR.text, 'html.parser')
                masteries = hotsoup.find_all("div", class_='panel-heading')
                for mastery in masteries:
                    if(len(mastery.contents) > 2):
                        ## nameAndLevel (nAL) will be a list of 2 items, the name of the mastery and it's corresponding level
                        nAL = mastery.contents[0].lstrip().split(" Level ")
                        mName = nAL[0]
                        for f in filterProfNames:
                            mName = mName.replace(f,'').rstrip()
                        if(len(mastery.contents[1]) == 1):
                            compEmbedValue = ""
                            compUrl = mastery.contents[1]['href']
                            #await self.bot.say(compUrl)
                            compnAL = mastery.contents[1].contents[0].split(" Mastery Level ")
                            if int(compnAL[1]) <= 1:
                                continue
                            compName = compnAL[0].rstrip()
                            compLevel = compnAL[1].lstrip()
                            compR = requests.get('https://talibri.com' + compUrl, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies2)
                            tomatoSoup = BeautifulSoup(compR.text, 'html.parser')
                            comps = tomatoSoup.find_all("div", class_='panel-heading')
                            for component in comps:
                                compMasterynAL = component.contents[0].lstrip().rstrip().split(" Creation Mastery Level ")
                                if len(compMasterynAL) <= 1 or compMasterynAL[1] == "1":
                                    continue
                                compEmbedValue += "{} {}\n".format(compMasterynAL[0],compMasterynAL[1].rstrip())
                            if compEmbedValue == "":
                                compEmbedValue = "[No Significant Masteries]"
                            embed.add_field(name="{} ({})".format(compnAL[0],compnAL[1]), value=compEmbedValue)
                        if len(nAL) > 1 and int(nAL[1]) > 1:
                            embedValue += "{} {}\n".format(mName,nAL[1].rstrip())
                if embedCounter < 24:
                    if embedValue == "":
                        embedValue = "[No Significant Masteries]"
                    embed.add_field(name=embedName, value=embedValue)
                    embedCounter += 1
                else:
                    embedCounterExceeded = True
        #newResults += "```"
        await self.bot.say(embed=embed)
        if embedCounterExceeded:
            await self.bot.say("There were more than 25 skills/component masteries found, and were trimmed.")
        #playerResults.append(newResults)
        #for message in playerResults:
        #    await self.bot.say(message)

    @talibri.command(name="sell_price")
    async def sell_price(self, *, itemName):
        """ Fetches the LOWEST sell price currently in the market for the item specified. For items with space, enclose the name with double quotes. """
        if len(itemName) < 3:
            await self.bot.say("Stop trolling me. Ban hammer strikes: 1")
            return

        lowestPrices = []
        url_market_types = ['material', 'raw-fish', 'food', 'herb', 'refined-material', 'ammunition', 'combat-potion', 'consumable-potion', 'finishing-material', 'gate', 'construction-material']
        #### Log into Talibri using the below credentials.
        #### We need to grab the cookies from the response and pass them to all subsequent requests to the server.
        email = 'asdfdelta@gmail.com'
        password = 'deltaforce'
        r = requests.get('https://talibri.com/users/sign_in')
        soup = BeautifulSoup(r.text, 'html.parser')
        utf = soup.find("input", attrs={'name': 'utf8'})['value']
        token = soup.find("input", attrs={'name': 'authenticity_token'})['value']
        cookies = r.cookies
        payload = {
            'user[email]': email,
            'user[password]': password,
            'user[remember_me]': '0',
            'commit': 'Log in',
            'utf8': utf,
            'authenticity_token': token
        }
        r = requests.post('https://talibri.com/users/sign_in', data=payload, cookies=cookies)
        if(r.status_code == 200):
            await self.bot.say("Logged in, fetching market information.")
        cookies = r.cookies
        total_section_best_listings = []
        ## Start scraping market pages
        for section in url_market_types:
            section_total_listings = []
            section_best_listings = []
            market_url = 'https://talibri.com/trade/1/get_listings?filter={}&order_type=sell'.format(section)
            r2 = requests.get(market_url, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies)
            market_parse = r2.text.split(".html(")[1].split(");")
            mplen = len(market_parse)
            market_parse = market_parse[:-1]
            mp_rebuild = ""
            for listing in market_parse:
                mp_rebuild += str(listing)
            market_parse = mp_rebuild.replace("\\","")
            hotsoup = BeautifulSoup(market_parse, 'html.parser')
            filters = hotsoup.find_all("tr", {"id": re.compile('^listing')})
            for listing in filters:
                listingid = listing['id'].split("listing-")[1]
                listingName = listing.contents[1].contents[0]
                quantity = listing.contents[3].contents[0]
                cost = listing.contents[5].contents[0]
                seller = listing.contents[7].contents[0]
                section_total_listings.append([listingName,int(cost.replace(',',''))])

            ## Find best prices for each listing.
            for item in section_total_listings:
                found = False
                for bidx, b_item in enumerate(section_best_listings):
                    if item[0] == b_item[0]:
                        found = True
                        if item[1] < b_item[1]:
                            section_best_listings[bidx] = item
                            continue
                if not found:
                    section_best_listings.append(item)

            total_section_best_listings.append([section,section_best_listings])
        found = False
        message = '```Best Sell Prices for items containing ' + itemName + ':\n'
        for sections in total_section_best_listings:
            for items in sections[1]:
                if itemName.lower() in items[0].lower():
                    found = True
                    message += items[0] + ': ' + str(items[1]) + "\n"
        message += '```'
        if not found:
            await self.bot.say("That item was not found on the market sell orders. As a reminder, I cannot search for components yet.")
        else:
            await self.bot.say(message)

    @talibri.command(name="buy_price")
    async def buy_price(self, *, itemName):
        """ Fetches the HIGHEST buy price currently in the market for the item specified. For items with space, enclose the name with double quotes. """
        if len(itemName) < 3:
            await self.bot.say("Stop trolling me. Ban hammer strikes: 1")
            return

        lowestPrices = []
        url_market_types = ['material', 'raw-fish', 'food', 'herb', 'refined-material', 'ammunition', 'combat-potion', 'consumable-potion', 'finishing-material', 'gate', 'construction-material']
        #### Log into Talibri using the below credentials.
        #### We need to grab the cookies from the response and pass them to all subsequent requests to the server.
        email = 'asdfdelta@gmail.com'
        password = 'deltaforce'
        r = requests.get('https://talibri.com/users/sign_in')
        soup = BeautifulSoup(r.text, 'html.parser')
        utf = soup.find("input", attrs={'name': 'utf8'})['value']
        token = soup.find("input", attrs={'name': 'authenticity_token'})['value']
        cookies = r.cookies
        payload = {
            'user[email]': email,
            'user[password]': password,
            'user[remember_me]': '0',
            'commit': 'Log in',
            'utf8': utf,
            'authenticity_token': token
        }
        r = requests.post('https://talibri.com/users/sign_in', data=payload, cookies=cookies)
        if(r.status_code == 200):
            await self.bot.say("Logged in, fetching market information.")
        cookies = r.cookies
        total_section_best_listings = []
        ## Start scraping market pages
        for section in url_market_types:
            section_total_listings = []
            section_best_listings = []
            market_url = 'https://talibri.com/trade/1/get_listings?filter={}&order_type=buy'.format(section)
            r2 = requests.get(market_url, headers={"X-Requested-With": "XMLHttpRequest"}, cookies=cookies)
            market_parse = r2.text.split(".html(")[1].split(");")
            mplen = len(market_parse)
            market_parse = market_parse[:-1]
            mp_rebuild = ""
            for listing in market_parse:
                mp_rebuild += str(listing)
            market_parse = mp_rebuild.replace("\\","")
            hotsoup = BeautifulSoup(market_parse, 'html.parser')
            filters = hotsoup.find_all("tr", {"id": re.compile('^listing')})
            for listing in filters:
                listingid = listing['id'].split("listing-")[1]
                listingName = listing.contents[1].contents[0]
                quantity = listing.contents[3].contents[0]
                cost = listing.contents[5].contents[0]
                seller = listing.contents[7].contents[0]
                section_total_listings.append([listingName,int(cost.replace(',',''))])

            ## Find best prices for each listing.
            for item in section_total_listings:
                found = False
                for bidx, b_item in enumerate(section_best_listings):
                    if item[0] == b_item[0]:
                        found = True
                        if item[1] > b_item[1]:
                            section_best_listings[bidx] = item
                            continue
                if not found:
                    section_best_listings.append(item)

            total_section_best_listings.append([section,section_best_listings])
        found = False
        message = '```Best Buy Prices for items containing ' + itemName + ':\n'
        for sections in total_section_best_listings:
            for items in sections[1]:
                if itemName.lower() in items[0].lower():
                    found = True
                    message += items[0] + ': ' + str(items[1]) + "\n"
        message += '```'
        if not found:
            await self.bot.say("That item was not found on the market sell orders. As a reminder, I cannot search for components yet.")
        else:
            await self.bot.say(message)

def setup(bot):
    if soupAvailable:
        bot.add_cog(Talibri(bot))
    else:
        raise RuntimeError("You need to run 'pip3 install beautifulsoup4'")
